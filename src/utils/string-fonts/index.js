const Montserrat_Bold = 'Montserrat-Bold';
const Montserrat_Medium = 'Montserrat-Medium';
const SF_UI_Text_Medium = 'SFUIText-Medium';
const SF_UI_Text_Regular = 'SFUIText-Regular'

export {
    Montserrat_Bold,
    Montserrat_Medium,
    SF_UI_Text_Medium,
    SF_UI_Text_Regular,
}
