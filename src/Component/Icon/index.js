import Entypo from 'react-native-vector-icons/Entypo'

import React from 'react';
import { StyleSheet } from 'react-native';

const Icon = ({
    name, backgroundColor
}) => (
    <Entypo name={name} size={48} color={backgroundColor} style={styles.icon}/>
);

export default Icon;
const styles = StyleSheet.create({
    icon: {
        paddingHorizontal: 18,
        paddingBottom: 33,
    }
});