import React from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign'

const ButtonIcon = (props) => {
    var { name, size, color, onPress } = props
    return (
        < TouchableOpacity style={styles.container} onPress = {onPress}>
            <AntDesign
                name={name}
                size={size}
                color={color}
            />
        </TouchableOpacity >
    )
}

export default ButtonIcon;
const styles = StyleSheet.create({
    container: {
        padding: 10,
    }
});