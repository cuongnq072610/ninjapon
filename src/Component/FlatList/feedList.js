import React from 'react'
import { View, FlatList, StyleSheet, Image, Text } from 'react-native'
import { SF_UI_Text_Regular } from '../../utils/string-fonts';
import ButtonIcon from '../ButtonIcon'

/**Sub component */
const FlatViewItem = (props) => {
    const { item } = props
    return (
        <View style={styles.flatItem}>
            {
                item.video ?
                    <Image
                        source={{ uri: `${item.video}` }}
                        style={{ height: 194, width: '100%' }}
                    /> : null
            }
            <View style={styles.content}>
                <View style={styles.headerItem}>
                    <Image
                        style={styles.avatar}
                        source={{ uri: `${item.avatar}` }}
                    />
                    <View style={styles.info}>
                        <Text style={{ color: '#1C2022', fontSize: 15, fontFamily: SF_UI_Text_Regular }}>{item.name}</Text>
                        <Text style={{ color: '#697882', fontSize: 12, fontFamily: SF_UI_Text_Regular }}>{item.twitter}</Text>
                    </View>
                    <ButtonIcon
                        name='twitter'
                        size={17}
                        color='#1DA1F2'
                        onPress={() => { }}
                    />
                </View>
                <Text
                    style={styles.body}
                >
                    {item.post.content}{item.post.subContent ? <Text style={{ color: '#2B7BB9' }}>{item.post.subContent}</Text> : ""}
                </Text>
                <Text style={styles.time}>
                    {item.time}
                </Text>
                <View style={styles.footer}>
                    <ButtonIcon
                        name='hearto'
                        size={15}
                        color='#657786'
                        onPress={() => { }}
                    />
                    <Text>{item.like}</Text>
                    <ButtonIcon
                        name='user'
                        size={15}
                        color='#657786'
                        onPress={() => { }}
                    />
                    <Text numberOfLines={1} style={{flex: 3}}>{`See ${item.name}'s other Twitter friends'`}</Text>
                    <ButtonIcon
                        name='infocirlce'
                        size={15}
                        color='#657786'
                        onPress={() => { }}
                    />
                </View>
            </View>
        </View>
    )
}

/**Main component */
const FlatView = (props) => {
    const {
        data
    } = props
    return (
        <View style={styles.container}>
            <FlatList
                data={data}
                renderItem={({ item, index }) => {
                    return (
                        <FlatViewItem item={item} index={index} />
                    )
                }}
                showsVerticalScrollIndicator={false}
            >
            </FlatList>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 10,
        marginTop: 10,
    },
    flatItem: {
        flex: 1,
        borderColor: '#E1E8ED',
        borderWidth: 1,
        marginBottom: 10,
    },
    content: {
        margin: 15,
    },
    headerItem: {
        height: 45,
        flexDirection: 'row',
    },
    avatar: {
        height: 40,
        width: 40,
        borderRadius: 20,
    },
    info: {
        flexDirection: 'column',
        marginLeft: 10,
        flex: 3
    },
    body: {
        color: '#1C2022',
        fontSize: 14,
        fontFamily: SF_UI_Text_Regular,
        marginTop: 10,
    },
    time: {
        color: '#697882',
        fontSize: 12,
        fontFamily: SF_UI_Text_Regular,
        marginVertical: 10,
    },
    footer: {
        alignItems: 'center',
        flexDirection: 'row',
        color: '#697882',
        fontFamily: SF_UI_Text_Regular,
        fontSize: 12,
    }
});

export default FlatView