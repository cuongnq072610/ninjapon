import React from 'react'
import { View, FlatList, StyleSheet, ImageBackground, Text } from 'react-native'
import ButtonIcon from '../ButtonIcon'

/**Sub component */
const FlatViewItem = (props) => {
    const { item } = props
    return (
        <View style={styles.flatItem}>
            <View style={styles.content}>
                <ImageBackground source={{ uri: `${item.video}` }} style={{ width: '100%', height: '100%' }}>
                    <View style={styles.icon}>
                        <ButtonIcon
                            name='playcircleo'
                            size={40}
                            color='#fff'
                            onPress={() => { }}
                        />
                    </View>
                    <View style={styles.text}>
                        <Text style={styles.text1}>{`${item.view} views`}</Text>
                        <Text style={styles.text2}>{item.min}</Text>
                    </View>
                </ImageBackground>
            </View>
            <View style={styles.footer}>
                <View style={styles.info}>
                    <ButtonIcon
                        name='closecircleo'
                        size={17}
                        color='#0148BC'
                        onPress={() => { }}
                    />
                    <Text>{item.kills}</Text>
                </View>
                <View style={styles.info}>
                    <ButtonIcon
                        name='user'
                        size={17}
                        color='#0148BC'
                        onPress={() => { }}
                    />
                    <Text>{item.type}</Text>
                </View>
                <View style={styles.info}>
                    <ButtonIcon
                        name='dashboard'
                        size={17}
                        color='#0148BC'
                        onPress={() => { }}
                    />
                    <Text>{`${item.timeUpload} min ago`}</Text>
                </View>
            </View>
        </View>
    )
}

/**Main component */
const FlatView = (props) => {
    const {
        data
    } = props
    return (

        <View style={styles.container}>
            <FlatList
                data={data}
                renderItem={({ item, index }) => {
                    return (
                        <FlatViewItem item={item} index={index} />
                    )
                }}
                showsVerticalScrollIndicator={false}
            >
            </FlatList>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 10,
        marginTop: 10,
    },
    flatItem: {
        flex: 1,
        marginBottom: 10,
        borderColor: '#E1E8ED',
        borderWidth: 1,
    },
    footer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    info: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
    },
    content: {
        height: 200,
        position: 'relative',
    },
    icon: {
        position: 'absolute',
        top: 70,
        left: 160,
    },
    text: {
        position: 'absolute',
        bottom: 0,
        flexDirection: 'row',
        marginBottom: 10,
    },
    text1: {
        flex: 1,
        color: 'white',
        marginLeft: 10,
    },
    text2: {
        flex: 1,
        color: 'white',
        textAlign: 'right',
        marginRight: 10,
    }
});

export default FlatView