import React from 'react';
import { TouchableOpacity, StyleSheet, Text } from 'react-native';
import { SF_UI_Text_Regular } from '../../utils/string-fonts';

const Btn = ({
    label, onClick, color, fontColor, border, borderColor
}) => (
        <TouchableOpacity onPress={onClick} style={[styles.btn, { backgroundColor: color ,  color: fontColor, borderWidth: border, borderColor: borderColor}]}>
            <Text style={{color: fontColor, fontSize: 17}}>{label}</Text>
        </TouchableOpacity>
    );

export default Btn;
const styles = StyleSheet.create({
    btn: {
        width: 311,
        height: 44,
        borderRadius: 24,
        alignItems: 'center',
        justifyContent: 'center',
        fontFamily: SF_UI_Text_Regular,
        marginBottom: 24,
    },
    
});