import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import ButtonIcon from '../ButtonIcon';

const Header = ({
    title, btnRight, btnLeft
}) => (
    <View style={styles.container}>
        <View style={styles.content}>
            <ButtonIcon 
                name={btnLeft.name}
                size={20}
                color={btnLeft.color}
                onPress={btnLeft.action}
            />
            <Text style={styles.title}>{title}</Text>
            <ButtonIcon 
                name={btnRight.name}
                size={20}
                color={btnRight.color}
                onPress={btnRight.action}
            />
        </View>
    </View>
);

export default Header;
const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 90,
        backgroundColor: '#152244',

    },
    content: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 40,
        paddingHorizontal: 10,
    },
    title: {
        fontSize: 17,
        color: 'white',
        marginTop: 8,
    }
});