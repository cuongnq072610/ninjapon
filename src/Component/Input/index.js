import React from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { TouchableOpacity, TextInput, StyleSheet } from 'react-native';

const InputField = ({
    label, onChange, value, password
}) => (
    <TouchableOpacity style={styles.btn}>
        <TextInput
            onChangeText={onChange}
            placeholder={label}
            value={value}
            style={StyleSheet.input}
            secureTextEntry={password}
        />
    </TouchableOpacity>
);

export default InputField;
const styles = StyleSheet.create({
    btn: {
        width: 311,
        borderBottomWidth: 1,
        borderBottomColor: "#9FB0CB",
        color: "#9FB0CB",
        fontSize: 16,
        marginBottom: 30,
        flexDirection: 'row',
    },
    input: {
        fontSize: 16,
        color: "#3E4A5D",
    }
});