import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import LinearGradient from 'react-native-linear-gradient';
import Btn from '../../Component/Button';
import InputField from '../../Component/Input';
import Icon from '../../Component/Icon'
class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
        this.onNavigate = this.onNavigate.bind(this)
    }

    onNavigate() {
        this.props.navigation.navigate('SignIn')
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.banner}>
                    <TouchableOpacity onPress={this.onNavigate}>
                        <Ionicons name={'ios-arrow-back'} size={25} color={'#9FB0CB'} />
                    </TouchableOpacity>
                    <Text style={styles.text2}>Create</Text>
                    <Text style={styles.text2}>new account</Text>
                </View>
                <View style={styles.content}>
                    <View style={styles.iconField}>
                        <Icon name={'facebook-with-circle'} backgroundColor='#4468B0' />
                        <Icon name={'twitter-with-circle'} backgroundColor='#3CBFF8' />
                        <Icon name={'instagram-with-circle'} />
                    </View>
                    <Text style={styles.text1}>Or Sign Up with email</Text>
                    <InputField label="Firstname" />
                    <InputField label="Lastname" />
                    <InputField label="Username" />
                    <InputField label="Password" password />
                    <View style={styles.buttonField}>
                        <Btn label="Sign Up" color="#111835" fontColor="#FFFFFF" />
                        <Btn label="Sign In" color="#FFFFFF" fontColor="#3E4A5D" border={1} borderColor='#9FB0CB' onClick={this.onNavigate} />
                    </View>
                </View>
            </View>
        );
    }
}

export default SignUp;
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    content: {
        alignItems: 'center'
    },
    iconField: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    text1: {
        color: '#9FB0CB',
        fontSize: 14,
        paddingBottom: 15
    },
    buttonField: {
        marginTop: 40
    },
    banner: {
        marginTop: 30,
        marginBottom: 30,
        marginLeft: 45,
    },
    text2: {
        alignContent: 'flex-start',
        fontSize: 32,
        color: '#3E4A5D',
    }
});
