import React, { Component } from 'react';
import LinearGradient from 'react-native-linear-gradient'
import { View, Text, StyleSheet, ImageBackground, StatusBar } from 'react-native'
import background from '../../assets/background.png'
import { Montserrat_Bold, Montserrat_Medium, SF_UI_Text_Medium } from '../../utils/string-fonts';
class Splash extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
        setTimeout(()=>{
            this.props.navigation.navigate("SignIn")
        },3000)
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar 
                    backgroundColor="transparent"
                    barStyle="light-content"
                    translucent={true}
                />
                <LinearGradient
                    colors={['#505FD2', '#9537BC', '#D42E76', '#F87E2F','#FDD97C']}
                    style={styles.linearGradient}
                    start={{ x: 1, y: 1 }}
                    end={{ x: 0, y: 0 }}
                >
                    <ImageBackground
                        source={background}
                        style={styles.imgBackGround}
                    />
                    <Text style={styles.text1}>NINJA</Text>
                    <Text style={styles.text2}>Team</Text>
                    <Text style={styles.text3}>An unofficial Ninja fan app</Text>
                </LinearGradient>
            </View>
        );
    }
}

export default Splash;
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    linearGradient: {
        flex: 1,
    },
    imgBackGround: {
        flex: 1,
        opacity: 0.2,
    },
    text1: {
        fontFamily: Montserrat_Bold,
        fontSize: 50,
        color: "#F9D513",
        position: 'absolute',
        alignSelf: 'center',
        top: 150,
    },
    text2: {
        fontFamily: Montserrat_Medium,
        color: "#F9D513",
        position: 'absolute',
        alignSelf: 'center',
        top: 210,
        fontSize: 25,
    },
    text3: {
        fontFamily: SF_UI_Text_Medium,
        color: 'rgba(255,255,255,0.5)',
        position: 'absolute',
        alignSelf: 'center',
        bottom: 15,
        fontSize: 14,
    },

});