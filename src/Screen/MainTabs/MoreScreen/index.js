import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import { SF_UI_Text_Regular } from '../../../utils/string-fonts';
import ButtonIcon from '../../../Component/ButtonIcon'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Feather from 'react-native-vector-icons/Feather'
import { Badge } from 'react-native-elements'

class MoreScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    this.navigateSignIn = this.navigateSignIn.bind(this)
    this.navigateSignUp = this.navigateSignUp.bind(this)
  }

  navigateSignIn() {
    this.props.navigation.navigate('SignIn')
  }

  navigateSignUp() {
    this.props.navigation.navigate('SignUp')
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.banner}>
          <Text style={styles.text2}> More </Text>
        </View>
        <View style={styles.header}>
          <View style={styles.text1}>
            <Text style={{ fontSize: 22, color: '#1F1F1F' }}>Hello Guest</Text>
            <Text style={{ color: '#696969' }}>Please sign in or sign up to unlock more features</Text>
          </View>
          <View style={styles.icon}>
            <ButtonIcon
              name='user'
              size={30}
              color='#0148BC'
            />
          </View>
        </View>
        <View style={styles.buttonField}>
          <TouchableOpacity style={[styles.button, { backgroundColor: '#5882F2' }]} onPress={() => { this.navigateSignIn }}>
            <Text style={{ color: '#ffffff' }}>Sign In</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.button, { backgroundColor: '#111835' }]} onPress={() => { this.navigateSignUp }}>
            <Text style={{ color: '#ffffff' }}>Sign Up</Text>
          </TouchableOpacity>
        </View>
        <Text style={{ fontSize: 22, color: '#1F1F1F' }}>Other</Text>
        <ScrollView>
          <View style={styles.option}>
            <MaterialIcons name='show-chart' size={25} color='#5882F2' />
            <Text style={{ color: '#383838', fontSize: 16, flex: 3, marginLeft: 10 }}>Ranking</Text>
            <Badge status="primary" value='news' containerStyle={{ marginRight: 20 }} />
            <Ionicons name='ios-arrow-forward' size={25} color='#D3D3D3' />
          </View>
          <View style={styles.option}>
            <Feather name='video' size={25} color='#5882F2' />
            <Text style={{ color: '#383838', fontSize: 16, flex: 3, marginLeft: 10 }}>Saving Video</Text>
            <Ionicons name='ios-arrow-forward' size={17} color='#D3D3D3' />
          </View>
          <View style={styles.option}>
            <Ionicons name='ios-recording' size={25} color='#5882F2' />
            <Text style={{ color: '#383838', fontSize: 16, flex: 3, marginLeft: 10 }}>Option 3</Text>
            <Ionicons name='ios-arrow-forward' size={17} color='#D3D3D3' />
          </View>
          <View style={styles.option}>
            <Feather name='mic' size={25} color='#5882F2' />
            <Text style={{ color: '#383838', fontSize: 16, flex: 3, marginLeft: 10 }}>Option 4</Text>
            <Ionicons name='ios-arrow-forward' size={17} color='#D3D3D3' />
          </View>
          <View style={styles.option}>
            <Feather name='file' size={25} color='#5882F2' />
            <Text style={{ color: '#383838', fontSize: 16, flex: 3, marginLeft: 10 }}>Option 5</Text>
            <Ionicons name='ios-arrow-forward' size={17} color='#D3D3D3' />
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default MoreScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: 20,
  },
  banner: {
    marginTop: 25,
    marginBottom: 35,
    fontFamily: SF_UI_Text_Regular,
  },
  text2: {
    fontSize: 32,
    color: '#3E4A5D',
  },
  header: {
    flexDirection: 'row',
  },
  text1: {
    flex: 3,
  },
  icon: {
    flex: 1,
    alignItems: 'flex-end',
    marginRight: 20,
  },
  buttonField: {
    flexDirection: 'row',
    marginTop: 37,
    justifyContent: 'space-around',
    marginBottom: 45,
  },
  button: {
    width: 160,
    height: 46,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  option: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginRight: 20,
    marginTop: 30,
  }
});