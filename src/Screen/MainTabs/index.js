import React from 'react';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import FeedScreen from './FeedScreen';
import WinScreen from './WinScreen';
import LiveScreen from './LiveScreen';
import ClipScreen from './ClipsScreen';
import MoreScreen from './MoreScreen';
import CustomTab from './customTab';

const RoutesConfig = {
    FeedScreen: {
        screen: FeedScreen
    },
    WinScreen: {
        screen: WinScreen
    },
    LiveScreen: {
        screen: LiveScreen
    },
    ClipScreen: {
        screen: ClipScreen
    },
    MoreScreen: {
        screen: MoreScreen
    }
}

const RoutesOption = {
    tabBarComponent: CustomTab
}

const mainTabs = createBottomTabNavigator(RoutesConfig, RoutesOption)

export default createAppContainer(mainTabs)