import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { SF_UI_Text_Medium } from '../../utils/string-fonts';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
class CustomTab extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    getTitleTab(index) {
        switch (index) {
            case 0:
                return "FEED";
            case 1:
                return "WINS"
            case 2:
                return "LIVE"
            case 3:
                return "CLIPS"
            case 4:
                return "MORE"
            default:
                break;
        }
    }
    getIcon(index, indexActive) {
        switch (index) {
            case 0:
                return <AntDesign name="twitter" size={17} color={index===indexActive?"#D73121":"#140F26"} />;
            case 1:
                return <MaterialIcons name="ondemand-video" size={17} color={index===indexActive?"#D73121":"#140F26"} />
            case 3:
                return <MaterialIcons name="video-library" size={17} color={index===indexActive?"#D73121":"#140F26"} />
            case 4:
                return <Feather name="more-horizontal" size={17} color={index===indexActive?"#D73121":"#140F26"} />
            default:
                break;
        }
    }
    goToScreen(key){
        this.props.navigation.navigate(key)
    }

    render() {
        var routes = this.props.navigation.state.routes;//routes la 1 mang gom 5 object
        var indexActive = this.props.navigation.state.index
        return (
            <View style={styles.container}>
                {
                    routes.map((item, index) => {
                        if (index !== 2) {
                            return (
                                <TouchableOpacity
                                    key={index}
                                    style={styles.btn}
                                    onPress = {()=>{this.goToScreen(item.key)}}
                                >
                                    {this.getIcon(index, indexActive)}
                                    <Text style={[styles.titleTab, {color: index===indexActive?"#D73121":"#140F26"}]}>{this.getTitleTab(index)}</Text>
                                </TouchableOpacity>
                            )
                        } else {
                            return (
                                <View
                                    style={styles.btn}
                                    key={index}
                                >
                                    <TouchableOpacity
                                        style={styles.btnLive}
                                        onPress = {()=>{this.goToScreen(item.key)}}
                                    >
                                        <View style={styles.cricle} />
                                        <Text style={[styles.titleTab, { marginTop: 0, color: 'white' }]}>LIVE</Text>
                                    </TouchableOpacity>
                                </View>

                            )
                        }

                    })
                }
            </View>
        );
    }
}

export default CustomTab;
const styles = StyleSheet.create({
    container: {
        width: "100%",
        backgroundColor: "white",
        flexDirection: 'row',
        borderTopWidth: 1,
        borderTopColor: "rgba(108,123,138,0.08)",
        paddingTop: 5,
        paddingBottom: 10,
    },
    btn: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    titleTab: {
        color: "#140F26",
        fontSize: 10,
        fontFamily: SF_UI_Text_Medium,
        marginTop: 5,
    },
    btnLive: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#D73121',
        borderRadius: 10,
        paddingHorizontal: 10,
        paddingVertical: 14,
    },
    cricle: {
        width: 8,
        height: 8,
        borderRadius: 4,
        backgroundColor: 'white',
        marginRight: 5,
    }

});