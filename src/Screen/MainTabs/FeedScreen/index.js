import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import Header from '../../../Component/Header';
import FlatView from '../../../Component/FlatList/feedList';
import mockData from '../../../../mockdata/data';


class FeedScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Header 
          title='@Ninja'
          btnLeft={{
            name:'twitter',
            color: '#1DA1F2',
            action: () => {}
          }}
          btnRight={{
            name:'infocirlceo',
            color: 'white',
            action: () => {}
          }}
        />
        <FlatView  data={mockData}/>
      </View>
    );
  }
}

export default FeedScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5F5F5'
  }
});
