import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Header from '../../../Component/Header';
import FlatView from '../../../Component/FlatList/winList'
import mockdata from '../../../../mockdata/winData';
class WinScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    console.log(mockdata)
    return (
      <View style={styles.container}>
        <Header
          title="Latest Win"
          btnLeft={{
            name: 'search1',
            color: 'white',
            action: () => { }
          }}
          btnRight={{
            name: 'bars',
            color: 'white',
            action: () => { }
          }}
        />
        <FlatView data={mockdata} />
      </View>
    );
  }
}

export default WinScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5F5F5'
  }
}); 