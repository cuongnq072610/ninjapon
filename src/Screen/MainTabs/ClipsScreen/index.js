import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import Header from '../../../Component/Header';
import FlatView from '../../../Component/FlatList/winList'
import mockdata from '../../../../mockdata/winData';
import Modal from "react-native-modal";
class ClipScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
  }

  toggle(){
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          title="Popular - 24h"
          btnLeft={{
            name: 'search1',
            color: 'white',
            action: () => { }
          }}
          btnRight={{
            name: 'bars',
            color: 'white',
            action: () => {this.toggle()}
          }}
        />
        {/* <Modal isVisible={this.state.isOpen}>
          <View style={{ flex: 1 }}>
            <Text>I am the modal content!</Text>
          </View>
        </Modal> */}
        <FlatView data={mockdata} />
      </View>
    );
  }
}

export default ClipScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1
  }
}); 
