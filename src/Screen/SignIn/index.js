import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Btn from '../../Component/Button';
import InputField from '../../Component/Input';
import Icon from '../../Component/Icon'
class componentName extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
        this.onNavigate = this.onNavigate.bind(this)
        this.onNavigateMaintabs = this.onNavigateMaintabs.bind(this)
    }

    onNavigate() {
        this.props.navigation.navigate('SignUp')
    }

    onNavigateMaintabs() {
        this.props.navigation.navigate('MainTabs')
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.banner}>
                    <Text style={styles.text2}>Hello</Text>
                    <Text style={styles.text2}>welcome back</Text>
                </View>
                <View style={styles.content}>
                    <View style={styles.iconField}>
                        <Icon name={'facebook-with-circle'} backgroundColor='#4468B0' />
                        <Icon name={'twitter-with-circle'} backgroundColor='#3CBFF8' />
                        <Icon name={'instagram-with-circle'} backgroundColor='#D42E76' />
                    </View>
                    <Text style={styles.text1}>Or Sign In with email</Text>
                    <InputField label="Username" />
                    <InputField label="Password" password />
                    <View style={styles.buttonField}>
                        <Btn label="Sign In" color="#5882F2" fontColor="#FFFFFF" onClick={this.onNavigateMaintabs}/>
                        <Btn label="Sign Up" color="#FFFFFF" fontColor="#3E4A5D" border={1} borderColor='#9FB0CB' onClick={this.onNavigate} />
                    </View>
                </View>
            </View>
        );
    }
}

export default componentName;
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    content: {
        alignItems: 'center'
    },
    iconField: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    text1: {
        color: '#9FB0CB',
        fontSize: 14,
        paddingBottom: 49
    },
    buttonField: {
        marginTop: 60
    },
    banner: {
        marginTop: 70,
        marginBottom: 35,
        marginLeft: 45,
    },
    text2: {
        alignContent: 'flex-start',
        fontSize: 32,
        color: '#3E4A5D',
    }
});