import { createStackNavigator, createAppContainer } from 'react-navigation'
import Splash from './Screen/Splash';
import SignIn from './Screen/SignIn'
import SignUp from './Screen/SignUp';
import MainTabs from './Screen/MainTabs';
const RouteConfigs = {
    Splash: {
        screen: Splash
    },
    SignIn: {
        screen: SignIn
    },
    SignUp: {
        screen: SignUp
    },
    MainTabs: {
        screen: MainTabs
    }
}

const AppNavigatorConfig = {
    initialRouteName: "Splash",//Sets the default screen of the stack. Must match one of the keys in route configs
    headerMode: "none"
}

const AppNavigator = createStackNavigator(RouteConfigs, AppNavigatorConfig)

export default createAppContainer(AppNavigator);